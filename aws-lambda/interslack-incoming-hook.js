/*
body: url-encoded, ampersand-delimeted
  token=xxxxxxxxxxxxxxxx
  team_id=T1234
  team_domain=one-word
  service_id=123
  channel_id=C123
  channel_name=random
  timestamp=1513733915.000105
  user_id=U123
  user_name=test_user
  text=outgoing+testing
  trigger_word=outgoing'

SLACK_SECRET_PARTNERS="{'team_domain'='slackteam','token':'XXXXXXX','channel':'partners'}"
*/

var https = require('https');
const querystring = require('querystring');


exports.handler = function(event, context, callback) {

    var message = querystring.parse(event.body);
    if(message.user_id == 'USLACKBOT') return callback();

    var body='';
    var useChannel = null;
    var useUser = null;
    var useText = null;
    var useTeam = null;
    var tokenIsValid = false;

    for(var envar in process.env) {
        if(envar.substr(0,12)=='SLACK_SECRET') {
            var secrets = JSON.parse(process.env[envar]);
            if(secrets.team_domain == message.team_domain
            && secrets.token == message.token) {
                tokenIsValid = true;

                if(secrets.channel != null) useChannel = secrets.channel;

                if(secrets.team_alias != null) useTeam = secrets.team_alias;
                else useTeam = message.team_domain;

                if(secrets.user_name != null) useUser = secrets.user_name;
                else useUser = message.user_name+" from "+useTeam;

                if(message.trigger_word
                && message.text.substr(0, message.trigger_word.length) === message.trigger_word)
                    useText = message.text.substr(message.trigger_word.length);
                else
                    useText = message.text;

                break;
            }
        }
    }

    if(tokenIsValid) {
        // the post options
        var optionspost = {
            host: process.env.SLACK_HOST,
            path: process.env.SLACK_PATH,
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            }
        };

        var reqPost = https.request(optionspost, function(res) {
            console.log("statusCode: ", res.statusCode);
            return callback();
        });

//        if(message.user_id == 'USLACKBOT')
//        	useText = 'Debug: '+ JSON.stringify(message);

        var req = {
            text: useText,
            username: useUser
        };

        if(useChannel != null) req.channel = useChannel;
        reqPost.write(JSON.stringify(req));
        reqPost.end();
    }
    else {
        return callback(null, {
            statusCode: 200,
            headers: {'Content-Type': 'application/json'},
            body: '{"text": "@'+ message.user_name +': Sorry, your message has been dropped. Contact the destination bot admin."}'
        });
    }
};



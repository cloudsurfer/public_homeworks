import cv2
import numpy as np

running = True
pixelSize = 48
pixelSpace = 16
numOfLeds = 32  # Don't make this larger than the width of the image
numOfRows = 1

rng_lo = np.array([0, 60, 80])
rng_hi = np.array([180, 255, 255])

roi_from = 1/4
roi_to = 3/4

# Creates the camera using its id (webcam id's are 0)
cam = cv2.VideoCapture(0)

while running:

    # Reads image, crops it, and flips it
    result, frame = cam.read()

    # Variable result is a boolean of whether or not the camera returned an image
    # This if statement prevents the code from crashing if the camera stops for a few frames
    if result:
        cam_rows = frame.shape[0]
        roi = frame[int(roi_from*cam_rows): int(roi_to*cam_rows)]
        # roi = cv2.flip(roi, 1) # This is only for testing with a webcam. Remove this in implimentation
        # Shrinks and pixelates the image
        small = cv2.resize(roi, (numOfLeds, numOfRows))
        hsv = cv2.cvtColor(small, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, rng_lo, rng_hi)
        hsv = cv2.add(hsv, (0, 160, 0))
        small = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)
        img = cv2.bitwise_and(small, small, mask=mask)

        led = np.zeros((pixelSize*numOfRows, pixelSize*numOfLeds, 3), np.uint8)
        led.fill(64)
        for i in range(len(img)):
            for j in range(len(img[i])):
                rect = np.array((j*pixelSize+pixelSpace, i*pixelSize+pixelSpace,
                                pixelSize-2*pixelSpace, pixelSize-2*pixelSpace))
                pixel = img[i][j]
                b = int(pixel[0])
                g = int(pixel[1])
                r = int(pixel[2])
                px = (b, g, r)
                cv2.rectangle(led, rect, px, -1)

        # Draws everything to the screen
        cv2.imshow("LED Strip", led)

    # Press 'q' to quit
    if cv2.waitKey(25) & 0xFF == ord('q'):
        running = False

cam.release()

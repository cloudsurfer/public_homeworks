import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.CvType;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class hello_opencv {
    static{ System.loadLibrary(Core.NATIVE_LIBRARY_NAME); }
    public static void main(String[] args) {
        System.out.println("Welcome to OpenCV " + Core.VERSION);

        // E.g. camera coordinates are X - to the right, Y - up, center is (0,0)
        Mat src = new Mat(4, 1, CvType.CV_32FC2);
        src.row(0).setTo(new Scalar( 1  , 1  ));
        src.row(1).setTo(new Scalar(-1  , 1  ));
        src.row(2).setTo(new Scalar( 0.5, 0.7)); // Farther is lower and closer to the center
        src.row(3).setTo(new Scalar(-0.5, 0.7)); // Farther is lower and closer to the center

        // And the real world: X - forward, Y - left, origin is the camera (meters)
        Mat dst = new Mat(4, 1, CvType.CV_32FC2, new Scalar(0));
        dst.row(0).setTo(new Scalar( 3,  1));
        dst.row(1).setTo(new Scalar( 3, -1));
        dst.row(2).setTo(new Scalar( 6,  1));
        dst.row(3).setTo(new Scalar( 6, -1));

        Mat p = Imgproc.getPerspectiveTransform(src, dst);
        System.out.println("Perspective Transform is:\n" + p);
        System.out.println("Perspective Transform data:\n" + p.dump());

        // Let's say camera detects a target at (x=0, y=0.8) on its screen
        Mat vision = new Mat(1,1, CvType.CV_32FC2, new Scalar(0,.8));
        Mat target = new Mat();
        Core.perspectiveTransform​(vision, target, p);
        System.out.println("Real target is at: " + target.dump() + " meters");
    }
}
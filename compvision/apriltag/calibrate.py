import numpy as np
import cv2 as cv
import glob
# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
nx=9
ny=6
#step=.86    # Width of my AprilTags is 43mm, checkers = 18.5 ---> Unit = 18.5 / (43/2) = 0.86
step = .0185 # in m # 73  # In inches
mx=nx*step
my=ny*step
objp = np.zeros((nx*ny,3), np.float32)
objp[:,:2] = np.mgrid[0:mx:step,0:my:step].T.reshape(-1,2)
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob('*.png')
for fname in images:
#    if '015' in fname or '016' in fname:
#        continue
    img = cv.imread(fname)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (nx,ny), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpoints.append(objp)
        corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners2)
        # Draw and display the corners
        cv.drawChessboardCorners(img, (nx,ny), corners2, ret)
        cv.imshow('img', img)
        cv.waitKey(30)
cv.destroyAllWindows()

ret, mtx, dist, rvecs, tvecs, stdi, stde, rms = cv.calibrateCameraExtended(objpoints, imgpoints, gray.shape[::-1], None, None)

print("Ret value:", ret)
print(mtx)
print("Distorsion:", dist)
print("RMS:", rms.T)
#, rvecs, tvecs)

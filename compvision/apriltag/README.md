# Raspberry Pi from scratch

`sudo apt install python3 python3-opencv`  
`sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 3`  

`sudo apt install cmake`  
`git clone https://github.com/AprilRobotics/apriltag.git`  
`cd apriltag`  
`cmake -B build -DCMAKE_BUILD_TYPE=Release`  
`cmake --build build --target install`  

By default Apriltag's build install the shared objects to `/usr/local/lib`.
This location should be already listed in one of the config files in `/etc/ld.so.conf.d/`.
If not then add it. In any case to add the so to the index run `sudo ldconfig` once.

## Try
You are going to need a `test.jpg` file with an apriltag of the matching family
(the argument string for the `apriltag()` constructor)

```
import cv2
import numpy as np
from apriltag import apriltag

imagepath = 'test.jpg'
image = cv2.imread(imagepath, cv2.IMREAD_GRAYSCALE)
detector = apriltag("tagStandard41h12")

detections = detector.detect(image)
```

## OpenCV from Git (to dev in C++)

https://pimylifeup.com/raspberry-pi-opencv/

`sudo apt install cmake build-essential pkg-config git`  
`sudo apt install libjpeg-dev libtiff-dev libjasper-dev libpng-dev libwebp-dev libopenexr-dev`  
`sudo apt install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev libxvidcore-dev libx264-dev libdc1394-22-dev libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev`  
`sudo apt install libgtk-3-dev libqt5gui5 libqt5webkit5 libqt5test5 python3-pyqt5`  
`sudo apt install libatlas-base-dev liblapacke-dev gfortran`  
`sudo apt install libhdf5-dev libhdf5-103`  
`sudo apt install python3-dev python3-pip python3-numpy`  

* Open /etc/dphys-swapfile with sudo
* Change `CONF_SWAPSIZE=100` to `CONF_SWAPSIZE=2048`

`sudo systemctl restart dphys-swapfile`  

`git clone https://github.com/opencv/opencv.git`
`git clone https://github.com/opencv/opencv_contrib.git`

* For the next command verify path for `OPENCV_EXTRA_MODULES_PATH`
* and check your Python version for `OPENCV_PYTHON_INSTALL_PATH`

```
cmake -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local \
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
    -D ENABLE_NEON=ON \
    -D ENABLE_VFPV3=ON \
    -D BUILD_TESTS=OFF \
    -D INSTALL_PYTHON_EXAMPLES=OFF \
    -D OPENCV_ENABLE_NONFREE=ON \
    -D CMAKE_SHARED_LINKER_FLAGS=-latomic \
    -D OPENCV_PYTHON_INSTALL_PATH=lib/python3.7/dist-packages \
    -D BUILD_EXAMPLES=OFF ..
```
`nproc` -- get the number of CPUs  
`make -j4` -- replace `4` with the actual number of CPUs  
`sudo make install`  
`sudo ldconfig`  

Revert back the `/etc/dphys-swapfile`

## libcamera
* add dtoverlay=imx219 to your /boot/config.txt file
`sudo apt install libcamera-apps`
`sudo apt install libcamera-dev`

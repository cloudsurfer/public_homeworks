import numpy as np
import cv2 as cv
import argparse


camera_id = "v4l2src device=/dev/video0 ! videoscale ! videorate ! video/x-raw,format=YUY2,width=1280,height=720 ! videoconvert ! appsink"

cap = cv.VideoCapture(camera_id)

cnt = 1

while(1):
    ret, frame = cap.read()
    if not ret:
        print('No frames grabbed!')
        break
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    # Display the resulting frame
    cv.imshow('frame', frame_gray)

    key = cv.pollKey()
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    if key & 0xFF == ord('q'):
        break

    if key & 0xFF == ord('c'):
        # Save current frame to a file
        file_name = f"calibrate_{cnt:03}.png"
        cv.imwrite(file_name, frame_gray)
        cnt += 1


# After the loop release the cap object
cap.release()
# Destroy all the windows
cv.destroyAllWindows()

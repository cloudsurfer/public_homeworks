import apriltag
import cv2
import numpy
import collections


""" As if chessboard was in Apriltag units, width of one apriltag = 2.0
tag_size = 43.0/25.4
camera_matrix = [
 [722.43973579,   0.        , 314.47536574],
 [  0.        , 719.42704588, 242.75961707],
 [  0.        ,   0.        ,   1.        ]]
distorsions = [[-2.13652915e-03, -1.97888796e-02,  5.83411556e-04, -7.53885456e-04, -7.05148118e-01]]
"""
# Camera calibrated in inches
tag_size = 1.8125
camera_matrix = [
 [722.43977102,   0.        , 314.47538051],
 [  0.        , 719.42710264, 242.75959433],
 [  0.        ,   0.        ,   1.        ]]
distorsions = [[-2.13647812e-03, -1.97886363e-02,  5.83429089e-04, -7.53879879e-04, -7.05147783e-01]]


camera_params = [camera_matrix[0][0], camera_matrix[1][1], camera_matrix[0][2], camera_matrix[1][2]]
cap = cv2.VideoCapture(0)

def draw_pose(overlay, camera_params, tag_size, pose, z_sign=1):

    opoints = numpy.array([
        -1, -1, 0,
         1, -1, 0,
         1,  1, 0,
        -1,  1, 0,
        -1, -1, -2*z_sign,
         1, -1, -2*z_sign,
         1,  1, -2*z_sign,
        -1,  1, -2*z_sign,
    ]).reshape(-1, 1, 3) * 0.5*tag_size

    edges = numpy.array([
        0, 1,
        1, 2,
        2, 3,
        3, 0,
        0, 4,
        1, 5,
        2, 6,
        3, 7,
        4, 5,
        5, 6,
        6, 7,
        7, 4
    ]).reshape(-1, 2)
        
    fx, fy, cx, cy = camera_params

    K = numpy.array([fx, 0, cx, 0, fy, cy, 0, 0, 1]).reshape(3, 3)

    rvec, _ = cv2.Rodrigues(pose[:3,:3])
    tvec = pose[:3, 3]

    dcoeffs = numpy.zeros(5)

    ipoints, _ = cv2.projectPoints(opoints, rvec, tvec, K, dcoeffs)

    ipoints = numpy.round(ipoints).astype(int)
    
    ipoints = [tuple(pt) for pt in ipoints.reshape(-1, 2)]

    for i, j in edges:
        cv2.line(overlay, ipoints[i], ipoints[j], (0, 255, 0), 1, 16)


while(1):
    ret, frame = cap.read()
    if not ret:
        print('No frames grabbed!')
        break

    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    detector = apriltag.Detector()
    detections, dimg = detector.detect(frame_gray, return_image=True)
    overlay = frame_gray // 2 + dimg // 2

    print_str = ''
    for i, detection in enumerate(detections):
        pose, e0, e1 = detector.detection_pose(detection, camera_params, tag_size)
        draw_pose(overlay, camera_params, tag_size, pose)
        print_str += detection.tostring(
            collections.OrderedDict([('Pose',pose), ('InitError', e0), ('FinalError', e1)]), indent=2)

    cv2.imshow('frame', overlay)

    key = cv2.pollKey()

    if key & 0xFF == ord('p'):
        print(detections)
        print(print_str)

    if key & 0xFF == ord('q'):
        break

# After the loop release the cap object
cap.release()
# Destroy all the windows
cv2.destroyAllWindows()
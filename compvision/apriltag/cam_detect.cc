#include <iostream>
#include <iomanip>

#include "opencv2/opencv.hpp"

extern "C" {
#include "apriltag/apriltag.h"
#include "apriltag/apriltag_pose.h"
#include "apriltag/tag36h11.h"
#include "apriltag/tag25h9.h"
#include "apriltag/tag16h5.h"
#include "apriltag/tagCircle21h7.h"
#include "apriltag/tagCircle49h12.h"
#include "apriltag/tagCustom48h12.h"
#include "apriltag/tagStandard41h12.h"
#include "apriltag/tagStandard52h13.h"
#include "apriltag/common/getopt.h"
}

using namespace std;
using namespace cv;

/*  For 1280x720 Logitech webcam:
Ret value: 0.16430218114248543
[[1.00480188e+03 0.00000000e+00 6.20728193e+02]
 [0.00000000e+00 1.00510972e+03 3.65756343e+02]
 [0.00000000e+00 0.00000000e+00 1.00000000e+00]]
Distorsion: [[ 0.01862513 -0.42585613  0.00245039 -0.00346462  0.5723603 ]]
RMS: [[0.1492185  0.18589305 0.15831873 0.17911458 0.18369869 0.17402952
  0.17006958 0.1475867  0.1602148  0.14031651 0.15337136 0.15707257
  0.15489589 0.15605624 0.17545612 0.1691228  0.15943331 0.17465047]]
*/

const double tagsize = (5.0 + 5.0/8 + 1.0/32)/0.937;
const double fx = 1.00480188e+03;
const double fy = 1.00510972e+03;
const double cx = 6.20728193e+02;
const double cy = 3.65756343e+02;

int main(int argc, char *argv[])
{
    getopt_t *getopt = getopt_create();

    getopt_add_bool(getopt, 'h', "help", 0, "Show this help");
    getopt_add_string(getopt, 'c', "camera", "v4l2src device=/dev/video0 ! videoscale ! videorate ! video/x-raw,format=YUY2,width=1280,height=720 ! videoconvert ! appsink", "camera ID");
    getopt_add_bool(getopt, 'd', "debug", 0, "Enable debugging output (slow)");
    getopt_add_bool(getopt, 'q', "quiet", 0, "Reduce output");
    getopt_add_string(getopt, 'f', "family", "tag16h5", "Tag family to use");
    getopt_add_int(getopt, 't', "threads", "1", "Use this many CPU threads");
    getopt_add_double(getopt, 'x', "decimate", "2.0", "Decimate input image by this factor");
    getopt_add_double(getopt, 'b', "blur", "0.0", "Apply low-pass blur to input");
    getopt_add_bool(getopt, '0', "refine-edges", 1, "Spend more time trying to align edges of tags");

    if (!getopt_parse(getopt, argc, argv, 1) ||
            getopt_get_bool(getopt, "help")) {
        printf("Usage: %s [options]\n", argv[0]);
        getopt_do_usage(getopt);
        exit(0);
    }

    cout << "Enabling video capture" << endl;

    TickMeter meter;
    meter.start();

    // Initialize camera
    VideoCapture cap(getopt_get_string(getopt, "camera"));
    if (!cap.isOpened()) {
        cerr << "Couldn't open video capture device" << endl;
        return -1;
    }

    // Initialize tag detector with options
    apriltag_family_t *tf = NULL;
    const char *famname = getopt_get_string(getopt, "family");
    if (!strcmp(famname, "tag36h11")) {
        tf = tag36h11_create();
    } else if (!strcmp(famname, "tag25h9")) {
        tf = tag25h9_create();
    } else if (!strcmp(famname, "tag16h5")) {
        tf = tag16h5_create();
    } else if (!strcmp(famname, "tagCircle21h7")) {
        tf = tagCircle21h7_create();
    } else if (!strcmp(famname, "tagCircle49h12")) {
        tf = tagCircle49h12_create();
    } else if (!strcmp(famname, "tagStandard41h12")) {
        tf = tagStandard41h12_create();
    } else if (!strcmp(famname, "tagStandard52h13")) {
        tf = tagStandard52h13_create();
    } else if (!strcmp(famname, "tagCustom48h12")) {
        tf = tagCustom48h12_create();
    } else {
        printf("Unrecognized tag family name. Use e.g. \"tag36h11\".\n");
        exit(-1);
    }


    apriltag_detector_t *td = apriltag_detector_create();
    apriltag_detector_add_family(td, tf);

    if (errno == ENOMEM) {
        printf("Unable to add family to detector due to insufficient memory to allocate the tag-family decoder with the default maximum hamming value of 2. Try choosing an alternative tag family.\n");
        exit(-1);
    }

    td->quad_decimate = getopt_get_double(getopt, "decimate");
    td->quad_sigma = getopt_get_double(getopt, "blur");
    td->nthreads = getopt_get_int(getopt, "threads");
    td->debug = getopt_get_bool(getopt, "debug");
    td->refine_edges = getopt_get_bool(getopt, "refine-edges");

    long frame_counter = 0L;
    meter.stop();
    cout << "Detector " << famname << " initialized in "
        << std::fixed << std::setprecision(3) << meter.getTimeSec() << " seconds" << endl;
#if CV_MAJOR_VERSION > 3
    cout << "  " << cap.get(CAP_PROP_FRAME_WIDTH ) << "x" <<
                    cap.get(CAP_PROP_FRAME_HEIGHT ) << " @" <<
                    cap.get(CAP_PROP_FPS) << "FPS" << endl;
#else
    cout << "  " << cap.get(CV_CAP_PROP_FRAME_WIDTH ) << "x" <<
                    cap.get(CV_CAP_PROP_FRAME_HEIGHT ) << " @" <<
                    cap.get(CV_CAP_PROP_FPS) << "FPS" << endl;
#endif
    meter.reset();

    Mat frame, gray;
    while (true) {
        errno = 0;
        cap >> frame;
        meter.start();
        frame_counter++;
        cvtColor(frame, gray, COLOR_BGR2GRAY);

        // Make an image_u8_t header for the Mat data
        image_u8_t im = { .width = gray.cols,
            .height = gray.rows,
            .stride = gray.cols,
            .buf = gray.data
        };

        zarray_t *detections = apriltag_detector_detect(td, &im);
        meter.stop();

        if (errno == EAGAIN) {
            printf("Unable to create the %d threads requested.\n",td->nthreads);
            exit(-1);
        }

        // Draw detection outlines
        stringstream ss;
        for (int i = 0; i < zarray_size(detections); i++) {
            apriltag_detection_t *det;
            zarray_get(detections, i, &det);
            ss << " Detected id:" << det->id;
            ss << ", c:[" << det->c[0] << "," << det->c[1] << "]";
            ss << endl;

            // First create an apriltag_detection_info_t struct using your known parameters.
            apriltag_detection_info_t info;
            info.det = det;
            info.tagsize = tagsize;
            info.fx = fx;
            info.fy = fy;
            info.cx = cx;
            info.cy = cy;

            // Then call estimate_tag_pose.
            apriltag_pose_t pose;
            double err = estimate_tag_pose(&info, &pose);
            ss << "Pose estimate err: " << err << endl;
            // Do something with pose.
            Mat R = Mat_<double>(3,3);
            Mat r;
            R.at<double>(0,0) = matd_get(pose.R, 0, 0);
            R.at<double>(1,0) = matd_get(pose.R, 1, 0);
            R.at<double>(2,0) = matd_get(pose.R, 2, 0);
            R.at<double>(0,1) = matd_get(pose.R, 0, 1);
            R.at<double>(1,1) = matd_get(pose.R, 1, 1);
            R.at<double>(2,1) = matd_get(pose.R, 2, 1);
            R.at<double>(0,2) = matd_get(pose.R, 0, 2);
            R.at<double>(1,2) = matd_get(pose.R, 1, 2);
            R.at<double>(2,2) = matd_get(pose.R, 2, 2);
            Rodrigues(R,r);
            ss << "Translation: [" << matd_get(pose.t, 0, 0);
            ss << "," << matd_get(pose.t, 1, 0);
            ss << "," << matd_get(pose.t, 2, 0);
            ss << "]" << endl;
            ss << r << endl;
        }
        apriltag_detections_destroy(detections);

        if (frame_counter % 16 == 0) {
                cout << "frame: " << frame_counter;
                cout << ", time: " << meter.getTimeSec();
                cout << ", avg: " << meter.getAvgTimeSec() << ", FPS: " << meter.getFPS();
                cout << endl << ss.str();
        }
        if (frame_counter > 64) {
                cout << frame_counter << " frames / " << meter.getTimeSec() << " = " << frame_counter / meter.getTimeSec() << endl;
                break;
        }
    }

    apriltag_detector_destroy(td);

    if (!strcmp(famname, "tag36h11")) {
        tag36h11_destroy(tf);
    } else if (!strcmp(famname, "tag25h9")) {
        tag25h9_destroy(tf);
    } else if (!strcmp(famname, "tag16h5")) {
        tag16h5_destroy(tf);
    } else if (!strcmp(famname, "tagCircle21h7")) {
        tagCircle21h7_destroy(tf);
    } else if (!strcmp(famname, "tagCircle49h12")) {
        tagCircle49h12_destroy(tf);
    } else if (!strcmp(famname, "tagStandard41h12")) {
        tagStandard41h12_destroy(tf);
    } else if (!strcmp(famname, "tagStandard52h13")) {
        tagStandard52h13_destroy(tf);
    } else if (!strcmp(famname, "tagCustom48h12")) {
        tagCustom48h12_destroy(tf);
    }


    getopt_destroy(getopt);

    return 0;
}


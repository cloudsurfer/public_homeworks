import apriltag
import cv2
img = cv2.imread('IMG_1384.JPG', cv2.IMREAD_GRAYSCALE)
detector = apriltag.Detector()
result = detector.detect(img)

print(result)

"""
det = [
#Detection(
    tag_family:'tag36h11', 
    tag_id=3, 
    hamming=0, 
    goodness=0.0, 
    decision_margin=75.450927734375, 
    homography=array(
        [   [-8.21568803e-01, -2.29144701e-01, -5.31083750e+00],
            [ 1.25185705e-02, -8.51696481e-01, -6.64054143e+00],
            [-1.15204006e-04, -6.17400394e-04, -1.71741946e-02]]), 
        center=array([309.23356972, 386.6580988 ]), 
        corners=array(
            [   [259.10656738, 352.84686279],
                [354.08242798, 346.46875   ],
                [355.25897217, 417.70275879],
                [266.93307495, 424.56384277]])
#)
]

print(det)
:w

"""